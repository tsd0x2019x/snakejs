/**
 * snake.js
 */

/**
 * Helper functions
 * @param {*} Id 
 */
function $(Id) {
    return document.getElementById(Id);
}

/**
 * Global constant(s) and variable(s)
 */
var Global = {
    Number : {
        INF : 65535, // User-defined value for infinity (2^16 - 1)
    },
    Directions : {
        HALT       : 0,
        MOVE_UP    : 1,
        MOVE_DOWN  : 2,
        TURN_LEFT  : 3,
        TURN_RIGHT : 4
    },
    Canvas : {
        Id : "MyCanvas",
        Height : window.innerHeight * 0.8, /* window.screen.height */
        Width  : window.innerWidth, /* window.screen.width */
    },
    Game : {
        Background : {
            Colors : ["#668cff", "#99b3ff"]
        },
        Collision : {
            Counter : { Id : "CollisionCounter" },
        },
        Running : false,
        Speed : 16.7, /* fps, 60Hz => 1000/60 [ms] = 16.66666...= 16.7 [ms] */
        SpeedFactor : 6,
    },
    Snake : { 
        Color  : "#80ff00", // Color of the snake's body
        Head : { Color : "#bfff80" }, // Color of the snake's head
        Length : {
            Counter : { Id : "SnakeLengthCounter" },
            Init_Value : 10, // (Initital/Start) Length of the snake's body
        },
    },
    Controls : {
        AI : { // Brain
            Color  : {
                ON  : "#b3ffb3",
                OFF : "red",
            },
            Id : "GameAI",
            Label  : { Id : "GameAI_Label" },
            ON  : true,
            OFF : false,
            Strategy : { // Different strategies for the snake AI movements
                Id : "GameAI_Strategy",
                Naive : "Naive",
                Heuristic : "Heuristic",
                Heuristic2 : "Heuristic2",
                Efficient : "Efficient",
            },
        },
        Buttons : {
            Id : "MyControlButtons",
            Images : {
                Folder : "images/",
                Files : {
                    ArrowDown  : "ArrowDown",
                    ArrowUp    : "ArrowUp",
                    ArrowLeft  : "ArrowLeft",
                    ArrowRight : "ArrowRight",
                    Play       : "PlayButton",
                    Pause      : "PauseButton"
                }
            }
        },
    },
    Cell : {
        Size : 15,  /* resolution (pixel) */
    },
    Nugget : {
        Color: "#ffff00", //Colors : ["#ffff00", "#ffffcc", "#e6ffe6"]
        Counter : { Id : "NuggetCounter" }
    },
};

/**
 * Class Point
 * @param {*} x 
 * @param {*} y 
 */
var Point = function (x, y) {
    this.x = x;
    this.y = y;
};

/**
 * Class Cell
 * @param {*} row 
 * @param {*} col 
 */
var Cell = function (row, col) {
    this.row = row;
    this.col = col;
};

/**
 * Class Origin
 */
var Origin = function () {
    this.canvas = $(Global.Canvas.Id); // document.getElementById(Global.Canvas.Id);
    this.canvas.width = Global.Canvas.Width;
    this.canvas.height = Global.Canvas.Height;
    this.context = this.canvas.getContext('2d');
    /* Methods */
    this.PaintRect = function (x, y, width, height, color) {
        this.context.fillStyle = color;
        this.context.fillRect(x, y, width, height);
    };
    this.PaintSquare = function (x, y, size, color) {
        this.PaintRect(x, y, size, size, color);
    };
    this.PaintLine = function (srcX, srcY, dstX, dstY, color) {
        this.context.lineWidth = 1;
        this.context.strokeStyle = color;
        this.context.beginPath();
        this.context.moveTo(srcX, srcY);
        this.context.lineTo(dstX, dstY);
        this.context.stroke();
        this.context.closePath();
    };
    this.Map = function (cell /* (row,col) */, Arr /* array */) { // Function that maps (row, col) index into (x,y) coordinate.
        // cell is a bundle of kind (row, col) that indicates the index (row, col) of a certain entry in array Arr.
        // This function provides the content of this entry with index (row, col) stored in cell.
        // For example: cell = (row,col) => Arr[row][col] = (x,y) will be returned.
        return Arr[cell.row][cell.col];
    };
    this.Map2 = function (row, col, Arr /* 2-dim array of coordinates */) { // Function that maps (row, col) index into (x,y)
        // Array Arr is a 2-dimensional array of coordinates. Each of its entry (cell) has index (row, col) and content (x,y).
        // For example: Arr[row][col] = (x,y)
        return Arr[row][col];
    };
    // Check if a cell (row,col) collides with other cells (row_i, col_i)
    this.CauseCollision = function (cell /* (row,col) */, ArrCells /* array of cells (row,col) */) {
        var IsCollided = false;
        for (var i = 0; i < ArrCells.length; i++) {
            if (cell.row == ArrCells[i].row && cell.col == ArrCells[i].col) {
                IsCollided = true;
                break;
            }
        }
        return IsCollided;
    };
};

/**
 * Class World (Model).
 * Let's say the game surface (World) has width W and height H. We consider it as an MxN matrix 
 * with (M * N) cells, where M := W/S, N := H/S and S is the size of each square cell. Every cell 
 * of that matrix has the index (row,col), where 0 <= row <= M-1 and 0 <= col <= N-1. The content
 * of a cell (row,col) represents the (x,y) coordinate, where 0 <= x <= W and 0 <= y <= H, i.e.
 * 
 *      cell(row,col) = (x,y)
 * 
 * The transformation between (row,col) and (x,y) is as follows:
 * 
 *          (row,col) => { x := (col * S), y := (row * S) }
 * 
 * In this program we use the variable name  S = Global.Cell.Size.
 */
var World = function () {
    Origin.call(this); // inherits from class Origin
    this.Rows = Math.floor(this.canvas.height / Global.Cell.Size) - 7;
    this.Cols = Math.floor(this.canvas.width / Global.Cell.Size);
    // Update to fill free gaps
    var FreeGaps = (this.canvas.width < 500 ? (this.canvas.width == 414 ? 1 : 2) : (this.canvas.width < 1000 ? 3 : (this.canvas.width < 1500 ? 4 : 6)));
    this.Cols -= FreeGaps; // FreeGaps are the free gaps that are caused by the white lines

    this.coords = []; // 2-dimensional array of coordinates. The element index is (row,col). Each element is a Point (x,y).
    this.nugget = new Cell(0,0); // (Initial) Loation (row,col) of nugget
    this.nugget_number = -1; // (Initial) Number of nugget(s) taken by snake
    this.IsNuggetPresent = false;

    var x, y = 0; // (x,y) coordinate
    var offsetX, offsetY = 0; // Gaps between the cell sqares
    for (var row = 0; row < this.Rows; row++) {
        this.coords.push([]); // create a (new) row
        x = 0;
        offsetX = 0;
        for (var col = 0; col < this.Cols; col++) {
            x = (col * Global.Cell.Size) + (offsetX++);
            y = (row * Global.Cell.Size) + offsetY;
            this.coords[row].push(new Point(x, y));
            //this.PaintSquare(x, y, Global.Cell.Size, Global.Game.Background.Colors[0]);
        }
        offsetY += 1;
    }

    this.CreateNugget = function () {
        this.nugget.row = Math.floor(Math.random() * (this.Rows-2)) + 1; // return a random number between 0 and this.Rows-1
        this.nugget.col = Math.floor(Math.random() * (this.Cols-2)) + 1; // return a random number between 0 and this.Cols-1
        this.nugget_number += 1;
    };

    this.GetNuggetNumber = function () {
        return this.nugget_number;
    };

    this.ResetNuggetNumber = function () { // Set initial number of nuggets to zero (0)
        this.nugget_number = -1;
    };

    this.PaintNugget = function () {
        var Coord = this.Map(this.nugget, this.coords); // transforms index (row,col) to coordinate (x,y)        
        //var NuggetColorIndex = Math.floor(Math.random() * (Global.Nugget.Colors.length-1)); // Random number between 0 and (Global.Nugget.Colors.length-1)
        //this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Nugget.Colors[NuggetColorIndex]);
        this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Nugget.Color);
    };

    this.Paint = function () { // Paint the whole world (model)
        y = 0; offsetY = 0;
        for (var row = 0; row < this.coords.length; row++) {
            x = 0;
            offsetX = 0;
            for (var col = 0; col < this.coords[0].length; col++) {
                x = (col * Global.Cell.Size) + (offsetX++);
                y = (row * Global.Cell.Size) + offsetY;
                if ((row + col) % 2 == 0) {
                    this.PaintSquare(x, y, Global.Cell.Size, Global.Game.Background.Colors[0]);
                }
                else {
                    this.PaintSquare(x, y, Global.Cell.Size, Global.Game.Background.Colors[1]);
                }
            }
            offsetY += 1;
        }
    };
};

/**
 * Class Snake
 * @param {*} WorldInstance
 * 
 *     OOOOOOOOOOOOOOOOOOOOOOO   = Array, Length = N
 *     ^                     ^
 *     |                     |
 *   tail (index N-1)        head (index 0)
 * 
 * The snake object has the following properties:
 *  {*} direction : The direction that the snake's head (and its body) is heading for.
 *  {*} length    : The snake's body (including its head) is represented by an array of squares cells.
 *                  The length of this array (number of the square cells) is the snake's length.
 *  {*} cells     : This is the one-dimensional array representing the snake (head and body). 
 *                  Every array element describes a square cell of the kind (row,column).
 *  {*} head      : The snake's head is represented by the first array element cells[0] with index 0.
 *  {*} tail      : Similar to the head, the snake's tail is the last array element cells[length-1],
 *                  where "length" is the snake's length.
 */ 
var Snake = function (WorldInstance /* Instance of class World */) {
    Origin.call(this); // Snake inherits from Origin.
    this.direction = Global.Directions.HALT; // (Current) Moving direction of the snake's head
    this.length = 0; // Snake length. Default 0
    this.cells = []; // 1-dimensional array of snake's cells. Each cell has the kind (row,col) as content.
    this.head = null; // The first element, i.e. this cells[0], is the snake' head.
    this.tail = null; // The last element, i.e. this.cells[N-1], is the snake's tail.
    // Property for collision detection
    this.collisions = 0;
    // Properties for approaching snake body
    this.approachingSide = null; // which side (beside snake head) the snake body is on.
    this.approachingMode = false;
    /* Methods */
    this.AddCellToArrayEnd = function (row, col) { // Row/Column index begins at 0 and ends at [Length-1].
        this.length = this.cells.push(new Cell(row,col)); // Adds a new cell (row,col) at the end of array and returns its new length
    };
    this.AddCellToArrayFront = function (row, col) {
        this.length = this.cells.unshift(new Cell(row,col)); // Adds a new cell (row,col) to the beginning of array and returns its new length
        this.head = this.cells[0]; // Update snake's head
    };
    this.RemoveLastCellFromArray = function () {
        var last = this.cells.pop(); // Remove and return the last cell (row,col) from array. The array length will be changed.
        this.length = this.cells.length; // Update snake's length
        return last;
    };
    this.CollidesWith = function (cell) { // Check if snake (head) collides with any cell (row,col)
        return (this.head.row == cell.row && this.head.col == cell.col) ? true : false;
    };
    this.GetLength = function () { // Return the current length of the snake
        return this.length;
    };
    this.GetNumberOfCollisions = function () { // Return the number of collisions between snake head and the obstacle
        return this.collisions;
    };
    this.ExistingInList = function (row, col, list) { // Checks if a given cell (row,col) is existing in the cell-list already
        var IsCellExistingInList = false;
        for (let ce of list) { // iterate over the values (cells) of list
            if (ce.row == row && ce.col == col) {
                IsCellExistingInList = true;
                break;
            }
        }
        return IsCellExistingInList;
    };
    this.Initialize = function () {
        if (Global.Snake.Length.Init_Value > 0) {
            if (this.cells.length > 0) { // If the array this.cells is not empty, empty it.
                do {
                    this.cells.pop(); // Remove last element from array
                } while (this.cells.length > 0);
                this.length = 0; // Reset the snake length to 0
            }
            for (var i = (Global.Snake.Length.Init_Value - 1); i >= 0; i--) {
                this.AddCellToArrayEnd(2, i + 2); // (row, col) = (2, i+2)
            }
            this.tail = this.cells[this.length-1];
            this.head = this.cells[0];
            this.direction = Global.Directions.TURN_RIGHT;
        }
    };
    this.MoveUp = function () { // 0: Halt, 1: MoveUp, 2: MoveDown, 3: TurnLeft, 4: TurnRight
        if (this.direction != Global.Directions.MOVE_DOWN) { // Not down
            this.direction = Global.Directions.MOVE_UP;
            //this.AddCellToArrayFront((this.head.row - 1) < 0 ? (WorldInstance.Rows - 1) : (this.head.row - 1), this.head.col);
            var next_row = ((this.head.row - 1) < 0 ? (WorldInstance.Rows - 1) : (this.head.row - 1));
            var next_col = this.head.col;
            if (!this.ExistingInList(next_row, next_col, this.cells)) {
                this.AddCellToArrayFront(next_row, next_col);
            }
            else { // Collision detected !
                this.collisions += 1;
                var Coord = this.Map2(next_row, next_col, WorldInstance.coords);
                this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, "red");
                
            }
            this.Update();
        }
    };
    this.MoveDown = function () { // 0: Halt, 1: MoveUp, 2: MoveDown, 3: TurnLeft, 4: TurnRight
        if (this.direction != Global.Directions.MOVE_UP) { // Not up
            this.direction = Global.Directions.MOVE_DOWN;
            //this.AddCellToArrayFront((this.head.row + 1) == WorldInstance.Rows ? 0 : (this.head.row + 1), this.head.col);
            var next_row = (this.head.row + 1) == WorldInstance.Rows ? 0 : (this.head.row + 1);
            var next_col = this.head.col;
            if (!this.ExistingInList(next_row, next_col, this.cells)) {
                this.AddCellToArrayFront(next_row, next_col);
            }
            else { // Collision detected !
                this.collisions += 1;
                var Coord = this.Map2(next_row, next_col, WorldInstance.coords);
                this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, "red");
            }
            this.Update();
        }    
    };
    this.MoveLeft = function () { // 0: Halt, 1: MoveUp, 2: MoveDown, 3: TurnLeft, 4: TurnRight
        if (this.direction != Global.Directions.TURN_RIGHT) { // Not right
            this.direction = Global.Directions.TURN_LEFT;
            //this.AddCellToArrayFront(this.head.row, ((this.head.col - 1) < 0) ? (WorldInstance.Cols - 1) : (this.head.col - 1));
            var next_row = this.head.row;
            var next_col = ((this.head.col - 1) < 0) ? (WorldInstance.Cols - 1) : (this.head.col - 1);
            if (!this.ExistingInList(next_row, next_col, this.cells)) {
                this.AddCellToArrayFront(next_row, next_col);
            }
            else { // Collision detected !
                this.collisions += 1;
                var Coord = this.Map2(next_row, next_col, WorldInstance.coords);
                this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, "red");
            }
            this.Update();
        }      
    };
    this.MoveRight = function () { // 0: Halt, 1: MoveUp, 2: MoveDown, 3: TurnLeft, 4: TurnRight
        if (this.direction != Global.Directions.TURN_LEFT) { // Not left
            this.direction = Global.Directions.TURN_RIGHT;
            //this.AddCellToArrayFront(this.head.row, ((this.head.col + 1) == WorldInstance.Cols) ? 0 : (this.head.col + 1));
            var next_row = this.head.row;
            var next_col = ((this.head.col + 1) == WorldInstance.Cols) ? 0 : (this.head.col + 1);
            if (!this.ExistingInList(next_row, next_col, this.cells)) {
                this.AddCellToArrayFront(next_row, next_col);
            }
            else { // Collision detected !
                this.collisions += 1;
                var Coord = this.Map2(next_row, next_col, WorldInstance.coords);
                this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, "red");
            }
            this.Update();
        }    
    };
    this.Update = function () {
        if (this.CollidesWith(WorldInstance.nugget)) { // Snake collides with nugget
            WorldInstance.IsNuggetPresent = false;
        }
        else {
            this.tail = this.RemoveLastCellFromArray(); // Remove last cell from array            
        }
        this.Repaint();
    };
    this.Repaint = function () {        
        // Clear tail (last cell) by paint it with backgroung color of WorldInstance.
        var Coord = this.Map(this.tail, WorldInstance.coords); // Maps from (row,col) to (x,y)
        if ((this.tail.row + this.tail.col) % 2 == 0) {
            WorldInstance.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Game.Background.Colors[0]);
        }
        else {
            WorldInstance.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Game.Background.Colors[1]);
        }
        // "Clear" the old head by painting it by the body color of Snake
        Coord = this.Map(this.cells[1], WorldInstance.coords);
        this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Snake.Color);
        // Paint the new inserted head (first cell) with the specific color for Snake's head
        Coord = this.Map(this.head, WorldInstance.coords);
        this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Snake.Head.Color);
    };
    this.Paint = function () {
        for (var i = 0; i < this.cells.length; i++) {
            var Coord = this.Map(this.cells[i], WorldInstance.coords);
            if (i == 0) { // snake's head
                this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Snake.Head.Color);
            }
            else {
                this.PaintSquare(Coord.x, Coord.y, Global.Cell.Size, Global.Snake.Color);
            }
        }
    };
    // Check if there is obstacle in front of the snake head. If true, return the index (greater than 0) of the first
    // obstacle, i.e. the cell of snake body that has the smallest distance to snake head. Otherwise, if there is 
    // no obstacle, return 0.
    this.IsThereObstacleInFront = function () {
        var index = 0;
        var distance = WorldInstance.Rows;
        switch (this.direction) {
            case Global.Directions.MOVE_UP:
                for (var i = 1; i < this.cells.length; i++) {
                    if (this.cells[i].row >= this.head.row) {
                        continue;
                    }
                    // this.cells[i].row < this.head.row
                    if (this.cells[i].col == this.head.col) {
                        if ((this.head.row - this.cells[i].row) < distance) {
                            index = i;
                            distance = this.head.row - this.cells[i].row;
                        }
                    }
                }
                break;
            case Global.Directions.MOVE_DOWN:
                for (var i = 1; i < this.cells.length; i++) {
                    if (this.cells[i].row <= this.head.row) {
                        continue;
                    }
                    // this.cells[i].row > this.head.row
                    if (this.cells[i].col == this.head.col) {
                        if ((this.cells[i].row - this.head.row) < distance) {
                            index = i;
                            distance = this.cells[i].row - this.head.row;
                        }
                    }
                }
                break;
            case Global.Directions.TURN_LEFT:
                distance = WorldInstance.Cols;
                for (var i = 1; i < this.cells.length; i++) {
                    if (this.cells[i].col >= this.head.col) {
                        continue;
                    }
                    // this.cells[i].col < this.head.col
                    if (this.cells[i].row == this.head.row) {
                        if ((this.head.col - this.cells[i].col) < distance) {
                            index = i;
                            distance = this.head.col - this.cells[i].col;
                        }
                    }
                }
                break;
            case Global.Directions.TURN_RIGHT:
                distance = WorldInstance.Cols;
                for (var i = 1; i < this.cells.length; i++) {
                    if (this.cells[i].col <= this.head.col) {
                        continue;
                    }
                    // this.cells[i].col > this.head.col
                    if (this.cells[i].row == this.head.row) {
                        if ((this.cells[i].col - this.head.col) < distance) {
                            index = i;
                            distance = this.cells[i].col - this.head.col;
                        }
                    }
                }
                break;
            default:
                break;
        }
        return index;
    };
    // Check if there is obstacle on the left side of the snake head. If true, return the index (greater than 0) of the 
    // first obstacle, i.e. the cell of snake body that has the smallest distance to snake head. Otherwise, if there is
    // no obstable, return 0.
    this.IsThereObstacleOnLeftSide = function () {
        var index = 0;
        var distance = WorldInstance.Cols;
        if (this.direction == Global.Directions.MOVE_UP || this.direction == Global.Directions.MOVE_DOWN) {
            for (var i = 1; i < this.cells.length; i++) {
                if (this.cells[i].col >= this.head.col) {
                    continue;
                }
                // this.cells[i].col < this.head.col
                if (this.cells[i].row == this.head.row) {
                    if ((this.head.col - this.cells[i].col) < distance) {
                        index = i;
                        distance = this.head.col - this.cells[i].col;
                    }
                }
            }
        }
        else if (this.direction == Global.Directions.TURN_LEFT) {
            distance = WorldInstance.Rows;
            for (var i = 1; i < this.cells.length; i++) {
                if (this.cells[i].row <= this.head.row) {
                    continue;
                }
                // this.cells[i].row > this.head.row
                if (this.cells[i].col == this.head.col) {
                    if ((this.cells[i].row - this.head.row) < distance) {
                        index = i;
                        distance = this.cells[i].row - this.head.row;
                    }
                }
            }
        }
        else if (this.direction == Global.Directions.TURN_RIGHT) {
            distance = WorldInstance.Rows;
            for (var i = 1; i < this.cells.length; i++) {
                if (this.cells[i].row >= this.head.row) {
                    continue;
                }
                // this.cells[i].row < this.head.row
                if (this.cells[i].col == this.head.col) {
                    if ((this.head.row - this.cells[i].row) < distance) {
                        index = i;
                        distance = this.head.row - this.cells[i].row;
                    }
                }
            }
        }
        return index;
    };
    // Check if there is obstacle on the right side of the snake head. If true, return the index (greater than 0) of the 
    // first obstacle, i.e. the cell of snake body that has the smallest distance to snake head. Otherwise, if there is
    // no obstable, return 0.
    this.IsThereObstacleOnRightSide = function () {
        var index = 0;
        var distance = WorldInstance.Rows;
        if (this.direction == Global.Directions.MOVE_UP || this.direction == Global.Directions.MOVE_DOWN) {
            for (var i = 1; i < this.cells.length; i++) {
                if (this.cells[i].col <= this.head.col) {
                    continue;
                }
                // this.cells[i].col > this.head.col
                if (this.cells[i].row == this.head.row) {
                    if ((this.cells[i].col - this.head.col) < distance) {
                        index = i;
                        distance = this.cells[i].col - this.head.col;
                    }
                }
            }
        }
        else if (this.direction == Global.Directions.TURN_LEFT) {
            for (var i = 1; i < this.cells.length; i++) {
                if (this.cells[i].row >= this.head.row) {
                    continue;
                }
                // this.cells[i].row < this.head.row
                if (this.cells[i].col == this.head.col) {
                    if ((this.head.row - this.cells[i].row) < distance) {
                        index = i;
                        distance = this.head.row - this.cells[i].row;
                    }
                }
            }
        }
        else if (this.direction == Global.Directions.TURN_RIGHT) {
            for (var i = 1; i < this.cells.length; i++) {
                if (this.cells[i].row <= this.head.row) {
                    continue;
                }
                // this.cells[i].row > this.head.row
                if (this.cells[i].col == this.head.col) {
                    if ((this.cells[i].row - this.head.row) < distance) {
                        index = i;
                        distance = this.cells[i].row - this.head.row;
                    }
                }
            }
        }
        return index;
    };
    // Returns the distance from the snake head to the obstacle in front. The distance is the number of free squares between the
    // snake head and the obstacle. If there is NO obstacle, return INFinity.
    this.GetDistanceToObstacleInFront = function () {
        var index = this.IsThereObstacleInFront();
        if (index == 0) {
            return Global.Number.INF;
        }
        else if (this.head.row == this.cells[index].row) {
            return Math.abs(this.head.col - this.cells[index].col) - 1;
        }
        else if (this.head.col == this.cells[index].col) {
            return Math.abs(this.head.row - this.cells[index].row) - 1;
        }
    };
    // Returns the distance from the snake head to the obstacle on left side. The distance is the number of free squares between the
    // snake head and the obstacle. If there is NO obstacle, return INFinity.
    this.GetDistanceToObstacleOnLeftSide = function () {
        var index = this.IsThereObstacleOnLeftSide();
        if (index == 0) {
            return Global.Number.INF;
        }
        else if (this.head.row == this.cells[index].row) {
            return Math.abs(this.head.col - this.cells[index].col) - 1;
        }
        else if (this.head.col == this.cells[index].col) {
            return Math.abs(this.head.row - this.cells[index].row) - 1;
        }
    };
    // Returns the distance from the snake head to the obstacle on right side. The distance is the number of free squares between the
    // snake head and the obstacle. If there is NO obstacle, return INFinity.
    this.GetDistanceToObstacleOnRightSide = function () {
        var index = this.IsThereObstacleOnRightSide();
        if (index == 0) {
            return Global.Number.INF;
        }
        else if (this.head.row == this.cells[index].row) {
            return Math.abs(this.head.col - this.cells[index].col) - 1;
        }
        else if (this.head.col == this.cells[index].col) {
            return Math.abs(this.head.row - this.cells[index].row) - 1;
        }
    };
    // Diese Funktion bewirkt, dass die Schlange bzw. der Schlangenkopf sich unmittelbar entlang des bzw. neben dem Körpers 
    // bewegt, bis Körperende erreicht wird. Dies wird das Herantasten genannt. Ausserdem wird anfangs vorausgesetzt, dass 
    // die Schlange sich bereits im Herantasten-Modus befindet.
    this.ApproachingMoveAlongBody = function (OnWhichSideIsBody) {
        var distanceToObstacleInFront = this.GetDistanceToObstacleInFront();
        var distanceToObstacleOnLeftSide = this.GetDistanceToObstacleOnLeftSide();
        var distanceToObstacleOnRightSide = this.GetDistanceToObstacleOnRightSide();
        if (OnWhichSideIsBody != undefined) { // Update property
            this.approachingSide = OnWhichSideIsBody;
        }
        switch (this.direction) {
            case Global.Directions.MOVE_UP:
                if (distanceToObstacleInFront == 0) {
                    if (this.approachingSide == Global.Directions.TURN_LEFT) { // Snake body is on left side of snake head.
                        this.approachingSide = Global.Directions.MOVE_UP;
                        this.MoveRight();
                    }
                    else if (this.approachingSide == Global.Directions.TURN_RIGHT) { // Snake body is on right side of snake head.
                        this.approachingSide = Global.Directions.MOVE_UP;
                        this.MoveLeft();
                    }
                }
                else { // (distanceToObstacleInFront == INF) || (distanceToObstacleInFront > 0 && distanceToObstacleInFront < INF)
                    distanceToObstacleOnLeftSide = this.GetDistanceToObstacleOnLeftSide();
                    distanceToObstacleOnRightSide = this.GetDistanceToObstacleOnRightSide();
                    if (distanceToObstacleOnLeftSide > 0 && distanceToObstacleOnRightSide > 0) {
                        this.approachingSide = Global.Directions.MOVE_DOWN;
                        if (this.approachingSide == Global.Directions.TURN_LEFT) {
                            this.MoveLeft();
                        }
                        else if (this.approachingSide == Global.Directions.TURN_RIGHT) {
                            this.MoveRight();
                        }
                    }
                    else {
                        this.MoveUp();
                    }
                }           
                break;
            case Global.Directions.MOVE_DOWN:
                if (distanceToObstacleInFront == 0) { // The obstacle is immediately in front of snake head.
                    if (this.approachingSide == Global.Directions.TURN_LEFT) { // Snake body is on left side of snake head.
                        this.approachingSide = Global.Directions.MOVE_DOWN;
                        this.MoveRight();
                    }
                    else if (this.approachingSide == Global.Directions.TURN_RIGHT) { // Snake body is on right side of snake head.
                        this.approachingSide = Global.Directions.MOVE_DOWN;
                        this.MoveLeft();
                    }
                }
                else { // (distanceToObstacleInFront == INF) || (distanceToObstacleInFront > 0 && distanceToObstacleInFront < INF)
                    distanceToObstacleOnLeftSide = this.GetDistanceToObstacleOnLeftSide();
                    distanceToObstacleOnRightSide = this.GetDistanceToObstacleOnRightSide();
                    if (distanceToObstacleOnLeftSide > 0 && distanceToObstacleOnRightSide > 0) {
                        this.approachingSide = Global.Directions.MOVE_UP;
                        if (this.approachingSide == Global.Directions.TURN_LEFT) {
                            this.MoveLeft();
                        }
                        else if (this.approachingSide == Global.Directions.TURN_RIGHT) {
                            this.MoveRight();
                        }
                    }
                    else {
                        this.MoveDown();
                    }
                }
                break;
            case Global.Directions.TURN_LEFT:
                if (distanceToObstacleInFront == 0) {
                    if (this.approachingSide == Global.Directions.MOVE_UP) { // Snake body is above snake head.
                        this.approachingSide = Global.Directions.TURN_LEFT;
                        this.MoveDown();
                    }
                    else if (this.approachingSide == Global.Directions.MOVE_DOWN) {
                        this.approachingSide = Global.Directions.TURN_LEFT;
                        this.MoveUp();
                    }
                }
                else { // (distanceToObstacleInFront == INF) || (distanceToObstacleInFront > 0 && distanceToObstacleInFront < INF)
                    distanceToObstacleOnLeftSide = this.GetDistanceToObstacleOnLeftSide();
                    distanceToObstacleOnRightSide = this.GetDistanceToObstacleOnRightSide();
                    if (distanceToObstacleOnLeftSide > 0 && distanceToObstacleOnRightSide > 0) {
                        this.approachingSide = Global.Directions.TURN_RIGHT;
                        if (this.approachingSide == Global.Directions.MOVE_UP) {
                            this.MoveUp();
                        }
                        else if (this.approachingSide == Global.Directions.MOVE_DOWN) {
                            this.MoveDown();
                        }
                    }
                    else {
                        this.MoveLeft();
                    }
                }
                break;
            case Global.Directions.TURN_RIGHT:
                if (distanceToObstacleInFront == 0) {
                    if (this.approachingSide == Global.Directions.MOVE_UP) { // Snake body is above snake head.
                        this.approachingSide = Global.Directions.TURN_RIGHT;
                        this.MoveDown();
                    }
                    else if (this.approachingSide == Global.Directions.MOVE_DOWN) { // Snake body is under snake head.
                        this.approachingSide = Global.Directions.TURN_RIGHT;
                        this.MoveUp();
                    }
                }
                else { // (distanceToObstacleInFront == INF) || (distanceToObstacleInFront > 0 && distanceToObstacleInFront < INF)
                    distanceToObstacleOnLeftSide = this.GetDistanceToObstacleOnLeftSide();
                    distanceToObstacleOnRightSide = this.GetDistanceToObstacleOnRightSide();
                    this.approachingSide = Global.Directions.TURN_LEFT;
                    if (distanceToObstacleOnLeftSide > 0 && distanceToObstacleOnRightSide > 0) {
                        if (this.approachingSide == Global.Directions.MOVE_UP) {
                            this.MoveUp();
                        }
                        else if (this.approachingSide == Global.Directions.MOVE_DOWN) {
                            this.MoveDown();
                        }
                    }
                    else {
                        this.MoveRight();
                    }
                }
                break;
            default:
                break;
        }
    };
};

/**
 * Class RunAnimation
 * @param {*} WorldInstance
 * @param {*} SnakeInstance 
 */
var RunAnimation = function (WorldInstance, SnakeInstance) {
    this.Strategy = { // includes different strategies for the snake AI on how to react in difficult situations (dead-end, close area, corner etc.).
        /*
         * "Naive" strategy is an intuitive approach that makes simple decisions and however does not cover the (bad) case where the snake can collides with its own body.
         */
        Naive : function (Is_AI_activated) {
            switch (SnakeInstance.direction) { // where the snake (head) is moving to
                case Global.Directions.MOVE_UP:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveUp();
                    }
                    else {
                        if (WorldInstance.nugget.row < SnakeInstance.head.row) {
                            SnakeInstance.MoveUp();
                        }
                        else { // (WorldInstance.nugget.row >= SnakeInstance.head.row) {
                            if (WorldInstance.nugget.col < SnakeInstance.head.col) {
                                SnakeInstance.MoveLeft();
                            }
                            else if (WorldInstance.nugget.col == SnakeInstance.head.col) {
                                SnakeInstance.MoveUp();
                            }
                            else { // (WorldInstance.nugget.col > SnakeInstance.head.col)
                                SnakeInstance.MoveRight();
                            }
                        }
                    }            
                    break;
                case Global.Directions.MOVE_DOWN:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveDown();
                    }
                    else {
                        if (WorldInstance.nugget.row > SnakeInstance.head.row) {
                            SnakeInstance.MoveDown();
                        }
                        else { // (WorldInstance.nugget.row <= SnakeInstance.head.row) {
                            if (WorldInstance.nugget.col < SnakeInstance.head.col) {
                                SnakeInstance.MoveLeft();
                            }
                            else if (WorldInstance.nugget.col == SnakeInstance.head.col) {
                                SnakeInstance.MoveDown();
                            }
                            else { // (WorldInstance.nugget.col > SnakeInstance.head.col)
                                SnakeInstance.MoveRight();
                            }
                        }
                    }
                    break;
                case Global.Directions.TURN_LEFT:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveLeft();
                    }
                    else {
                        if (WorldInstance.nugget.col < SnakeInstance.head.col) {
                            SnakeInstance.MoveLeft();
                        }
                        else { // (WorldInstance.nugget.col >= SnakeInstance.head.col) {
                            if (WorldInstance.nugget.row < SnakeInstance.head.row) {
                                SnakeInstance.MoveUp();
                            }
                            else if (WorldInstance.nugget.row == SnakeInstance.head.row) {
                                SnakeInstance.MoveLeft();
                            }
                            else { // (WorldInstance.nugget.row > SnakeInstance.head.row)
                                SnakeInstance.MoveDown();
                            }
                        }
                    }
                    break;
                case Global.Directions.TURN_RIGHT:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveRight();
                    }
                    else {
                        if (WorldInstance.nugget.col > SnakeInstance.head.col) {
                            SnakeInstance.MoveRight();
                        }
                        else { // (WorldInstance.nugget.col <= SnakeInstance.head.col) {
                            if (WorldInstance.nugget.row < SnakeInstance.head.row) {
                                SnakeInstance.MoveUp();
                            }
                            else if (WorldInstance.nugget.row == SnakeInstance.head.row) {
                                SnakeInstance.MoveRight();
                            }
                            else { // (WorldInstance.nugget.row > SnakeInstance.head.row)
                                SnakeInstance.MoveDown();
                            }
                        }
                    }
                    break;
                default:
                    break;
            } // end-switch
        }, // Naive
        /*
         * "Heuristic" strategy is an advanced version of "Naive" strategy.
         */
        Heuristic : function (Is_AI_activated) {
            var distanceHeadToNugget = 0;
            switch (SnakeInstance.direction) { // where the snake (head) is moving to
                case Global.Directions.MOVE_UP:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveUp();
                    }
                    else {
                        if (WorldInstance.nugget.row < SnakeInstance.head.row) {
                            SnakeInstance.MoveUp();
                        }
                        else { // (WorldInstance.nugget.row >= SnakeInstance.head.row) {
                            if (WorldInstance.nugget.col < SnakeInstance.head.col) {
                                //SnakeInstance.MoveLeft();
                                distanceHeadToNugget = SnakeInstance.head.col - WorldInstance.nugget.col;
                                if (distanceHeadToNugget > ((WorldInstance.Cols - SnakeInstance.head.col) + WorldInstance.nugget.col)) {
                                    SnakeInstance.MoveRight();
                                }
                                else {
                                    SnakeInstance.MoveLeft();
                                }
                            }
                            else if (WorldInstance.nugget.col == SnakeInstance.head.col) {
                                //SnakeInstance.MoveUp();
                                distanceHeadToNugget = WorldInstance.nugget.row - SnakeInstance.head.row;
                                if (distanceHeadToNugget > ((WorldInstance.Rows - WorldInstance.nugget.row) + SnakeInstance.head.row)) {
                                    SnakeInstance.MoveUp();
                                }
                                else {
                                    if (SnakeInstance.GetDistanceToObstacleOnLeftSide() >= SnakeInstance.GetDistanceToObstacleOnRightSide()) {
                                        SnakeInstance.MoveLeft();
                                    }
                                    else {
                                        SnakeInstance.MoveRight();
                                    }
                                    SnakeInstance.MoveDown();
                                }
                            }
                            else { // (WorldInstance.nugget.col > SnakeInstance.head.col)
                                //SnakeInstance.MoveRight();
                                distanceHeadToNugget = WorldInstance.nugget.col - SnakeInstance.head.col;
                                if (distanceHeadToNugget > ((WorldInstance.Cols - WorldInstance.nugget.col) + SnakeInstance.head.col)) {
                                    SnakeInstance.MoveLeft();
                                }
                                else {
                                    SnakeInstance.MoveRight();
                                }
                            }
                        }
                    }            
                    break;
                case Global.Directions.MOVE_DOWN:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveDown();
                    }
                    else {
                        if (WorldInstance.nugget.row > SnakeInstance.head.row) {
                            SnakeInstance.MoveDown();
                        }
                        else { // (WorldInstance.nugget.row <= SnakeInstance.head.row) {
                            if (WorldInstance.nugget.col < SnakeInstance.head.col) {
                                //SnakeInstance.MoveLeft();
                                distanceHeadToNugget = SnakeInstance.head.col - WorldInstance.nugget.col;
                                if (distanceHeadToNugget > ((WorldInstance.Cols - SnakeInstance.head.col) + WorldInstance.nugget.col)) {
                                    SnakeInstance.MoveRight();
                                }
                                else {
                                    SnakeInstance.MoveLeft();
                                }
                            }
                            else if (WorldInstance.nugget.col == SnakeInstance.head.col) {
                                //SnakeInstance.MoveDown();
                                distanceHeadToNugget = SnakeInstance.head.row - WorldInstance.nugget.row;
                                if (distanceHeadToNugget > ((WorldInstance.Rows - SnakeInstance.head.row) + WorldInstance.nugget.row)) {
                                    SnakeInstance.MoveDown();
                                }
                                else {
                                    if (SnakeInstance.GetDistanceToObstacleOnLeftSide() >= SnakeInstance.GetDistanceToObstacleOnRightSide()) {
                                        SnakeInstance.MoveLeft();
                                    }
                                    else {
                                        SnakeInstance.MoveRight();
                                    }
                                    SnakeInstance.MoveUp();
                                }
                            }
                            else { // (WorldInstance.nugget.col > SnakeInstance.head.col)
                                //SnakeInstance.MoveRight();
                                distanceHeadToNugget = WorldInstance.nugget.col - SnakeInstance.head.col;
                                if (distanceHeadToNugget > ((WorldInstance.Cols - WorldInstance.nugget.col) + SnakeInstance.head.col)) {
                                    SnakeInstance.MoveLeft();
                                }
                                else {
                                    SnakeInstance.MoveRight();
                                }
                            }
                        }
                    }
                    break;
                case Global.Directions.TURN_LEFT:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveLeft();
                    }
                    else {
                        if (WorldInstance.nugget.col < SnakeInstance.head.col) {
                            SnakeInstance.MoveLeft();
                        }
                        else { // (WorldInstance.nugget.col >= SnakeInstance.head.col) {
                            if (WorldInstance.nugget.row < SnakeInstance.head.row) {
                                //SnakeInstance.MoveUp();
                                distanceHeadToNugget = SnakeInstance.head.row - WorldInstance.nugget.row;
                                if (distanceHeadToNugget > ((WorldInstance.Rows - SnakeInstance.head.row) + WorldInstance.nugget.row)) {
                                    SnakeInstance.MoveDown();
                                }
                                else {
                                    SnakeInstance.MoveUp();
                                }
                            }
                            else if (WorldInstance.nugget.row == SnakeInstance.head.row) {
                                //SnakeInstance.MoveLeft();
                                distanceHeadToNugget = WorldInstance.nugget.col - SnakeInstance.head.col;
                                if (distanceHeadToNugget > ((WorldInstance.Cols - WorldInstance.nugget.col) + SnakeInstance.head.col)) {
                                    SnakeInstance.MoveLeft();
                                }
                                else {
                                    if (SnakeInstance.GetDistanceToObstacleOnLeftSide() >= SnakeInstance.GetDistanceToObstacleOnRightSide()) {
                                        SnakeInstance.MoveDown();
                                    }
                                    else {
                                        SnakeInstance.MoveUp();
                                    }
                                    SnakeInstance.MoveRight();
                                }
                            }
                            else { // (WorldInstance.nugget.row > SnakeInstance.head.row)
                                //SnakeInstance.MoveDown();
                                distanceHeadToNugget = WorldInstance.nugget.row - SnakeInstance.head.row;
                                if (distanceHeadToNugget > ((WorldInstance.Rows - WorldInstance.nugget.row) + SnakeInstance.head.row)) {
                                    SnakeInstance.MoveUp();
                                }
                                else {
                                    SnakeInstance.MoveDown();
                                }
                            }
                        }
                    }
                    break;
                case Global.Directions.TURN_RIGHT:
                    if (Is_AI_activated == undefined || !Is_AI_activated) {
                        SnakeInstance.MoveRight();
                    }
                    else {
                        if (WorldInstance.nugget.col > SnakeInstance.head.col) {
                            SnakeInstance.MoveRight();
                        }
                        else { // (WorldInstance.nugget.col <= SnakeInstance.head.col) {
                            if (WorldInstance.nugget.row < SnakeInstance.head.row) {
                                //SnakeInstance.MoveUp();
                                distanceHeadToNugget = SnakeInstance.head.row - WorldInstance.nugget.row;
                                if (distanceHeadToNugget > ((WorldInstance.Rows - SnakeInstance.head.row) + WorldInstance.nugget.row)) {
                                    SnakeInstance.MoveDown();
                                }
                                else {
                                    SnakeInstance.MoveUp();
                                }
                            }
                            else if (WorldInstance.nugget.row == SnakeInstance.head.row) {
                                //SnakeInstance.MoveRight();
                                distanceHeadToNugget = SnakeInstance.head.col - WorldInstance.nugget.col;
                                if (distanceHeadToNugget > ((WorldInstance.Cols - SnakeInstance.head.col) + WorldInstance.nugget.col)) {
                                    SnakeInstance.MoveRight();
                                }
                                else {
                                    if (SnakeInstance.GetDistanceToObstacleOnLeftSide() >= SnakeInstance.GetDistanceToObstacleOnRightSide()) {
                                        SnakeInstance.MoveUp();
                                    }
                                    else {
                                        SnakeInstance.MoveDown();
                                    }
                                    SnakeInstance.MoveLeft();
                                }
                            }
                            else { // (WorldInstance.nugget.row > SnakeInstance.head.row)
                                //SnakeInstance.MoveDown();
                                distanceHeadToNugget = WorldInstance.nugget.row - SnakeInstance.head.row;
                                if (distanceHeadToNugget > ((WorldInstance.Rows - WorldInstance.nugget.row) + SnakeInstance.head.row)) {
                                    SnakeInstance.MoveUp();
                                }
                                else {
                                    SnakeInstance.MoveDown();
                                }
                            }
                        }
                    }
                    break;
                default:
                    break;
            } // end-switch
        }, // Heuristic
        /*
         * Efficient
         */
        Efficient : function (Is_AI_activated) {
            // TODO    
        }, // Efficient
    }; // Strategy
    this.Launch = function (Is_AI_activated, Strategy_Name) {
        if (Strategy_Name == undefined) {
            Strategy_Name = Global.Controls.AI.Strategy.Naive;
        }
        switch (Strategy_Name) {
                case Global.Controls.AI.Strategy.Naive:
                    this.Strategy.Naive(Is_AI_activated);
                    break;
                case Global.Controls.AI.Strategy.Heuristic:
                    this.Strategy.Heuristic(Is_AI_activated);
                    break;
                case Global.Controls.AI.Strategy.Efficient:
                    this.Strategy.Efficient(Is_AI_activated);
                    break;
                default:
                    break;
        }
    };
};

/**
 * Class ControlSwitch
 * @param {*} SnakeInstance 
 * @param {*} GameInstance 
 */
var ControlSwitch = function (SnakeInstance, GameInstance) {
    this.Launch = function (event_name_of_key_or_click, play_pause_button) {
        switch (event_name_of_key_or_click) { // from button attribute "name" with value e.g. "ArrowUp" etc.
            case "ArrowUp":
                SnakeInstance.MoveUp();
                this.RunIfNotStartedYet(play_pause_button);
                break;
            case "ArrowDown":
                SnakeInstance.MoveDown();
                this.RunIfNotStartedYet(play_pause_button);
                break;
            case "ArrowLeft":
                SnakeInstance.MoveLeft();
                this.RunIfNotStartedYet(play_pause_button);
                break;
            case "ArrowRight":
                SnakeInstance.MoveRight();
                this.RunIfNotStartedYet(play_pause_button);
                break;
            default:
                break;
        }
    };    
    this.RunIfNotStartedYet = function (play_pause_button) {
        if (!GameInstance.IsRunning) {
            play_pause_button.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files["Pause"] + ".png)"; // "url(images/PauseButton.png)";
            GameInstance.Play();
        }
    };
};

/**
 * Class Controller
 * @param {*} WorldInstance 
 * @param {*} SnakeInstance 
 * @param {*} GameInstance 
 */
var Controller = function (WorldInstance, SnakeInstance, GameInstance) {
    SnakeInstance.Initialize();
    WorldInstance.Paint();
    SnakeInstance.Paint();
    var ControlSwitchInstance = new ControlSwitch(SnakeInstance, GameInstance);
    var RunAnimationInstance = new RunAnimation(WorldInstance, SnakeInstance);

    // Control buttons
    var control_buttons = $(Global.Controls.Buttons.Id).querySelectorAll("button"); // document.getElementById(Global.Controls.Buttons.Id).querySelectorAll("button");
    var play_pause_button = control_buttons[control_buttons.length-1]; // Get the last button

    // AI checkbox and label
    var brain_checkbox = $(Global.Controls.AI.Id); // document.getElementById(Global.Controls.AI.Id); // checkbox
    var brain_label = $(Global.Controls.AI.Label.Id); // document.getElementById(Global.Controls.AI.Label.Id); // label
    var brain_strategy = $(Global.Controls.AI.Strategy.Id);
    brain_label.style.background = Global.Controls.AI.Color.OFF;
    brain_strategy.options[0].selected = true;

    // Counters
    var collision_counter_text = $(Global.Game.Collision.Counter.Id); // document.getElementById(Global.Game.Collision.Counter.Id);
    var nugget_counter_text = $(Global.Nugget.Counter.Id);
    var snake_length_counter_text = $(Global.Snake.Length.Counter.Id);

    // (Static internal function) Switch AI label between states ON/OFF
    function TurnAILabel (New_AI_State) {
        if (New_AI_State == Global.Controls.AI.ON) {
            brain_checkbox.checked = true;
            brain_label.innerHTML = "AI is ON";
            brain_label.style.background = Global.Controls.AI.Color.ON;
        }
        else {
            brain_checkbox.checked = false;
            brain_label.innerHTML = "AI is OFF";
            brain_label.style.background = Global.Controls.AI.Color.OFF;
        }
    }

    // MouseEvent
    window.addEventListener("mouseover", function (evt) {
        var current = evt.target;
        if (current.nodeName == "BUTTON") {
            if (current.name != "PlayPause") { // HTML attribute "name" with value "PlayPause"
                current.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files[current.name] + "_hover.png)"; // "url(images/" + current.name + "_hover.png)";
            }
        }
    }, false);

    // MouseEvent
    window.addEventListener("mouseout", function (evt) {
        var current = evt.target;
        if (current.nodeName == "BUTTON") {
            if (current.name != "PlayPause") {
                current.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files[current.name] + ".png)"; // "url(images/" + current.name + ".png)";
            }
        }
    }, false);

    // MouseEvent
    window.addEventListener("click", function (evt) {
        var current = evt.target;
        if (current.nodeName == "BUTTON") { // BUTTON
            if (current.name != "PlayPause") {
                if (brain_checkbox.checked) {
                    TurnAILabel(Global.Controls.AI.OFF);
                    GameInstance.Pause();
                }
                ControlSwitchInstance.Launch(current.name, play_pause_button);
            }
            else { // (current.name == "PlayPause") { // BUTTON attribute (property) "name" with value "PlayPause"
                if (GameInstance.IsRunning) {
                    play_pause_button.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files["Play"] + ".png)"; // "url(images/PlayButton.png)";
                    GameInstance.Pause();
                }
                else {
                    play_pause_button.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files.Pause + ".png)"; // "url(images/PauseButton.png)";
                    GameInstance.Play(brain_checkbox.checked ? Global.Controls.AI.ON : Global.Controls.AI.OFF);
                }
            }
        }
        else if (current.nodeName == "INPUT") { // HTML Checkbox
            if (current.checked) { // Activate snake's brain
                GameInstance.Pause();
                TurnAILabel(Global.Controls.AI.ON);
                play_pause_button.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files["Pause"] + ".png)"; // "url(images/PauseButton.png)";
                GameInstance.Play(Global.Controls.AI.ON);
            }
            else {
                TurnAILabel(Global.Controls.AI.OFF);
                var Local_Game_IsRunning = GameInstance.IsRunning;
                GameInstance.Pause();
                if (Local_Game_IsRunning) {
                    GameInstance.Play();
                }
            }
        }
        else if (current.nodeName == "OPTION" || current.tagName == "OPTION") { // HTML selection
            switch (current.value) {
                case Global.Controls.AI.Strategy.Naive:
                    GameInstance.AI_Strategy = Global.Controls.AI.Strategy.Naive;
                    break;
                case Global.Controls.AI.Strategy.Heuristic:
                    GameInstance.AI_Strategy = Global.Controls.AI.Strategy.Heuristic;
                    break;
                case Global.Controls.AI.Strategy.Heuristic2:
                    //GameInstance.AI_Strategy = Global.Controls.AI.Strategy.Heuristic2;
                    break;
                case Global.Controls.AI.Strategy.Efficient:
                    //GameInstance.AI_Strategy = Global.Controls.AI.Strategy.Efficient;
                    break;
                default:
                    break;
            }
        }
        current.blur(); // Remove focus on the (recently clicked) element (!)
    }, false);

    // KeyboardEvent
    window.addEventListener("keydown", function (evt) {
        var key = evt.key || evt.code;
        /* +===================+====================+
         * | KeyboardEvent.key | KeyboardEvent.code |
         * +===================+====================+
         * |    ArrowUp        |    ArrowUp         |
         * |    ArrowDown      |    ArrowDown       |
         * |    ArrowLeft      |    ArrowLeft       |
         * |    ArrowRight     |    ArrowRight      |
         * |    a              |    KeyA            |
         * |    b              |    KeyB            |
         * |    (empty)        |    Space           |
         * +===================+====================+
         */
        //ControlSwitchInstance.Launch(key, play_pause_button);    
        if ((key == " ") || (key == "Space")) { // Spacebar
            if (GameInstance.IsRunning) {
                play_pause_button.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files.Play + ".png)"; // "url(images/PlayButton.png)";
                GameInstance.Pause();
            }
            else {
                play_pause_button.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files.Pause + ".png)"; // "url(images/PauseButton.png)";
                GameInstance.Play(brain_checkbox.checked ? Global.Controls.AI.ON : Global.Controls.AI.OFF);
            }
        }
        else { // Other key
            if (brain_checkbox.checked) {
                TurnAILabel(Global.Controls.AI.OFF);
                GameInstance.Pause();
            }
            ControlSwitchInstance.Launch(key, play_pause_button);
        }
    }, false);

    // Run animation
    this.Run = function (Is_AI_activated) {
        var Delay = Global.Game.Speed * Global.Game.SpeedFactor;
        GameInstance.Id = window.setInterval(function () { // 0: Halt, 1: MoveUp, 2: MoveDown, 3: TurnLeft, 4: TurnRight
            if (!WorldInstance.IsNuggetPresent) {
                do {
                    WorldInstance.CreateNugget(); // (row, col)
                } while (WorldInstance.CauseCollision(WorldInstance.nugget, SnakeInstance.cells));
                WorldInstance.IsNuggetPresent = true;
                WorldInstance.PaintNugget();
                nugget_counter_text.innerHTML = "" + WorldInstance.GetNuggetNumber();
            }
            snake_length_counter_text.innerHTML = "" + SnakeInstance.GetLength();
            collision_counter_text.innerHTML = "" + SnakeInstance.GetNumberOfCollisions();
            // Check if the snake head collides with its body and shrinks to a length (e.g. 9) less than Global.Snake.Length.Init_Value (10)
            // If true ==> GAME OVER ==> RESTART
            if (SnakeInstance.length < Global.Snake.Length.Init_Value) {
                TurnAILabel(Global.Controls.AI.OFF); // Reset (Unset) AI checkbox
                GameInstance.Pause(); // Set the "IsRunning" variable to "false". Stop the game flow.
                SnakeInstance.Initialize(); // Reset snake to initial state
                WorldInstance.ResetNuggetNumber(); // Set number of nuggets to zero (0)
                WorldInstance.IsNuggetPresent = false;
                // Repaint game objects
                WorldInstance.Paint(); // Repaint world
                SnakeInstance.Paint(); // Repaint snake
                play_pause_button.style.backgroundImage = "url(" + Global.Controls.Buttons.Images.Folder + Global.Controls.Buttons.Images.Files["Play"] + ".png)"; // "url(images/PlayButton.png)";
                alert("GAME OVER!!\nSnake length is less than 10.\nRESTART!!");
                // Reset counters
                nugget_counter_text.innerHTML = "0";
                collision_counter_text.innerHTML = "0";
                snake_length_counter_text.innerHTML = "" + Global.Snake.Length.Init_Value;
            }
            else {
                //RunAnimationInstance.Launch(Is_AI_activated);
                RunAnimationInstance.Launch(Is_AI_activated, GameInstance.AI_Strategy);
            }
        }, Delay);
    };
};

/**
 * Class Game
 */
var Game = function () {
    this.Id = null;
    this.IsRunning = false;
    this.AI_Strategy = null;

    var MyWorld = new World();
    var MySnake = new Snake(MyWorld);
    var MyController = new Controller(MyWorld, MySnake, this);

    this.Pause = function () {
        this.IsRunning = false;
        window.clearInterval(this.Id);
    };

    this.Play = function (Is_AI_activated) {
        this.IsRunning = true;
        MyController.Run(Is_AI_activated);
    };

    this.Start = function () {
        if (this.IsRunning) {
            MyController.Run();
        }
    };
};

/**
 * Main Program
 */
var MyGame = new Game();
MyGame.Start();
